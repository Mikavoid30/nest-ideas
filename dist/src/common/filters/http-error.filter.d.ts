import { ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';
import { QueryFailedError } from 'typeorm';
export declare class HttpErrorFilter implements ExceptionFilter {
    catch(exception: EntityNotFoundError | QueryFailedError | Error, host: ArgumentsHost): void;
    private static getCustomStatus;
}
