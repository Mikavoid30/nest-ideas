"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var HttpErrorFilter_1;
const common_1 = require("@nestjs/common");
const EntityNotFoundError_1 = require("typeorm/error/EntityNotFoundError");
const typeorm_1 = require("typeorm");
let HttpErrorFilter = HttpErrorFilter_1 = class HttpErrorFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const { url: path, method } = ctx.getRequest();
        const statusCode = exception instanceof common_1.HttpException
            ? exception.getStatus()
            : HttpErrorFilter_1.getCustomStatus(exception);
        let { message } = exception || { message: null };
        let errorMessage = message instanceof Object ? message.message : exception.message;
        const errorObject = {
            statusCode,
            timestamp: new Date().toISOString(),
            path,
            method,
            errors: errorMessage
        };
        common_1.Logger.error(`[${method}][${path}][${statusCode}]`, exception.message, 'HttpErrorFilter');
        response.status(statusCode).json(errorObject);
    }
    static getCustomStatus(exception) {
        let status = common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        if (exception instanceof EntityNotFoundError_1.EntityNotFoundError)
            status = common_1.HttpStatus.UNPROCESSABLE_ENTITY;
        if (exception instanceof typeorm_1.QueryFailedError)
            status = common_1.HttpStatus.NOT_FOUND;
        return status;
    }
};
HttpErrorFilter = HttpErrorFilter_1 = __decorate([
    common_1.Catch(EntityNotFoundError_1.EntityNotFoundError, typeorm_1.QueryFailedError, Error)
], HttpErrorFilter);
exports.HttpErrorFilter = HttpErrorFilter;
//# sourceMappingURL=http-error.filter.js.map