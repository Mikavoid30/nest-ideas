import { CanActivate, ExecutionContext } from '@nestjs/common';
export declare class AuthGuard implements CanActivate {
    canActivate(context: ExecutionContext): Promise<boolean>;
    validateRequest(request: any): Promise<any>;
    validateToken(auth: string): Promise<string | object>;
}
