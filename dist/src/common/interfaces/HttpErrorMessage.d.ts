export interface HttpErrorObject {
    statusCode: number;
    timestamp: string;
    path: string;
    method: string;
    errors: Array<any> | Object | string;
}
