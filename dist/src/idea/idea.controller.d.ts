import { IdeaDTO } from './idea.dto';
import { IdeaService } from './idea.service';
export declare class IdeaController {
    private ideaService;
    constructor(ideaService: IdeaService);
    showAllIdeas(): Promise<import("./idea.dto").IdeaRO[]>;
    createIdea(data: IdeaDTO, userId: any): Promise<import("./idea.dto").IdeaRO>;
    readIdea(id: string): Promise<import("./idea.dto").IdeaRO>;
    updateIdea(id: string, userId: string, data: IdeaDTO): Promise<import("./idea.dto").IdeaRO>;
    destroyIdea(id: string, userId: string): Promise<{
        deleted: string;
    }>;
}
