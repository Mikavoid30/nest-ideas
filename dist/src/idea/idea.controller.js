"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const idea_dto_1 = require("./idea.dto");
const common_1 = require("@nestjs/common");
const idea_service_1 = require("./idea.service");
const validation_pipe_1 = require("../common/pipes/validation.pipe");
const user_decorator_1 = require("src/user/user.decorator");
const auth_guard_1 = require("../common/guards/auth.guard");
let IdeaController = class IdeaController {
    constructor(ideaService) {
        this.ideaService = ideaService;
    }
    showAllIdeas() {
        return this.ideaService.showAll();
    }
    createIdea(data, userId) {
        return this.ideaService.create(data, userId);
    }
    readIdea(id) {
        return this.ideaService.read(id);
    }
    updateIdea(id, userId, data) {
        return this.ideaService.update(id, data, userId);
    }
    destroyIdea(id, userId) {
        return this.ideaService.destroy(id, userId);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], IdeaController.prototype, "showAllIdeas", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    common_1.UsePipes(validation_pipe_1.ValidationPipe),
    __param(0, common_1.Body()), __param(1, user_decorator_1.User('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [idea_dto_1.IdeaDTO, Object]),
    __metadata("design:returntype", void 0)
], IdeaController.prototype, "createIdea", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], IdeaController.prototype, "readIdea", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id')),
    __param(1, user_decorator_1.User('id')),
    __param(2, common_1.Body(new common_1.ValidationPipe({ skipMissingProperties: true }))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, idea_dto_1.IdeaDTO]),
    __metadata("design:returntype", void 0)
], IdeaController.prototype, "updateIdea", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(auth_guard_1.AuthGuard),
    __param(0, common_1.Param('id')), __param(1, user_decorator_1.User('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", void 0)
], IdeaController.prototype, "destroyIdea", null);
IdeaController = __decorate([
    common_1.Controller('api/ideas'),
    __metadata("design:paramtypes", [idea_service_1.IdeaService])
], IdeaController);
exports.IdeaController = IdeaController;
//# sourceMappingURL=idea.controller.js.map