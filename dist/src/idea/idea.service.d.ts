import { Repository } from 'typeorm';
import { IdeaEntity } from './idea.entity';
import { IdeaDTO, IdeaRO } from './idea.dto';
import { UserEntity } from '../user/user.entity';
export declare class IdeaService {
    private ideaRepository;
    private userRepository;
    constructor(ideaRepository: Repository<IdeaEntity>, userRepository: Repository<UserEntity>);
    private toResponseObject;
    private ensureOwnership;
    create(data: IdeaDTO, userId: string): Promise<IdeaRO>;
    showAll(): Promise<IdeaRO[]>;
    read(id: string): Promise<IdeaRO>;
    update(id: string, data: Partial<IdeaDTO>, userId: string): Promise<IdeaRO>;
    destroy(id: string, userId: string): Promise<{
        deleted: string;
    }>;
}
