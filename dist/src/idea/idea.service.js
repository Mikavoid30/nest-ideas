"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const idea_entity_1 = require("./idea.entity");
const typeorm_2 = require("@nestjs/typeorm");
const user_entity_1 = require("../user/user.entity");
let IdeaService = class IdeaService {
    constructor(ideaRepository, userRepository) {
        this.ideaRepository = ideaRepository;
        this.userRepository = userRepository;
    }
    toResponseObject(idea) {
        const res = Object.assign({}, idea, { author: idea.author && idea.author.toResponseObject(false) });
        if (res.upvotes)
            res.upvotes = idea.upvotes.length;
        if (res.downvotes)
            res.downvotes = idea.downvotes.length;
        return res;
    }
    ensureOwnership(idea, userId) {
        if (idea.author.id !== userId) {
            throw new common_1.UnauthorizedException('Incorrect user');
        }
        return true;
    }
    create(data, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.findOne({ id: userId });
            const idea = yield this.ideaRepository.create(data);
            idea.author = user;
            this.ideaRepository.save(idea);
            return this.toResponseObject(idea);
        });
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const ideas = yield this.ideaRepository.find({
                relations: ['author']
            });
            return ideas.map((idea) => this.toResponseObject(idea));
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const idea = yield this.ideaRepository.findOne({
                where: { id },
                relations: ['author']
            });
            if (!idea) {
                throw new common_1.NotFoundException(`cannot found idea n°${id}`);
            }
            return this.toResponseObject(idea);
        });
    }
    update(id, data, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.findOne({ id: userId });
            const idea = yield this.ideaRepository.findOne({
                where: { id },
                relations: ['author']
            });
            this.ensureOwnership(idea, user.id);
            if (!user || !idea) {
                throw new common_1.NotFoundException(`cannot found idea n°${id}`);
            }
            yield this.ideaRepository.save(Object.assign({}, idea, data));
            return this.toResponseObject(Object.assign({}, idea, data));
        });
    }
    destroy(id, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const toDelete = yield this.ideaRepository.findOne({
                where: { id },
                relations: ['author']
            });
            this.ensureOwnership(toDelete, userId);
            if (!toDelete) {
                throw new common_1.NotFoundException(`cannot found idea n°${id}`);
            }
            yield this.ideaRepository.delete(id);
            return { deleted: toDelete.id };
        });
    }
};
IdeaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(idea_entity_1.IdeaEntity)),
    __param(1, typeorm_2.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository])
], IdeaService);
exports.IdeaService = IdeaService;
//# sourceMappingURL=idea.service.js.map