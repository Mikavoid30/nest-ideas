import { IdeaEntity } from '../idea/idea.entity';
export default class UserDTO {
    readonly username: string;
    readonly password: string;
}
export declare class UserRO {
    id: string;
    created: Date;
    username: string;
    token?: string;
    ideas?: IdeaEntity[];
    bookmarks?: IdeaEntity[];
}
