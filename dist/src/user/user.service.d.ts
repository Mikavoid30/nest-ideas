import { UserEntity } from './user.entity';
import { Repository } from 'typeorm';
import UserDTO from './user.dto';
import { UserRO } from './user.dto';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<UserEntity>);
    login(data: UserDTO): Promise<UserRO>;
    register(data: UserDTO): Promise<UserRO>;
    showAll(): Promise<UserRO[]>;
}
