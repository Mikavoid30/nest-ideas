const faker = require('faker')
const axios = require('axios')

const IDEA_GENERATOR = 'https://appideagenerator.com/call.php'
const IDEA_API = 'http://localhost:3000'

const generateIdea = async () => {
  const { data } = await axios.get(IDEA_GENERATOR)
  return data.replace(/\n/g, '')
}

const generateUser = async () => {
  const { data } = await axios.post(`${IDEA_API}/register`, {
    username: faker.internet.userName(),
    password: 'password'
  })

  return data.token
}

const postNewIdea = async (token) => {
  const idea = await generateIdea()
  const { data } = await axios.post(`${IDEA_API}/api/ideas`, {
    idea,
    description: faker.lorem.paragraph()
  }, {
    headers: {
      authorization: `Bearer ${token}`
    }
  })

  console.log('Data added', data)
}

(async function main() {
  const nb = 100
  for (let i = 0; i < nb; i++) {
    const token = await generateUser()
    postNewIdea(token)
  }
})()