import {
  Controller,
  Param,
  Get,
  Post,
  Delete,
  Body,
  UseGuards,
  UsePipes
} from '@nestjs/common'
import { CommentService } from './comment.service'
import { User } from 'dist/src/user/user.decorator'
import { CommentDTO } from './comment.dto'
import { AuthGuard } from '../common/guards/auth.guard'
import { ValidationPipe } from '../common/pipes/validation.pipe'

@Controller('api/comments')
export class CommentController {
  constructor(private commentService: CommentService) {}

  @Get('ideas/:id')
  showCommentsByIdea(@Param('id') ideaId: string) {
    return this.commentService.showAllFromIdea(ideaId)
  }

  @Get('users/:id')
  showCommentsByUser(@Param('id') userId: string) {
    return this.commentService.showAllFromUser(userId)
  }

  @Post('ideas/:id')
  @UseGuards(AuthGuard)
  @UsePipes(ValidationPipe)
  postCommentToIdea(
    @Param('id') ideaId: string,
    @User('id') currentUserId: string,
    @Body() data: CommentDTO
  ) {
    return this.commentService.postToIdea(ideaId, currentUserId, data)
  }

  @Delete(':id')
  @UseGuards(AuthGuard)
  deleteComment(@Param('id') commentId: string, @User('id') userId: string) {
    return this.commentService.destroy(commentId, userId)
  }

  @Get(':id')
  showComment(@Param('id') commentId: string) {
    return this.commentService.read(commentId)
  }
}
