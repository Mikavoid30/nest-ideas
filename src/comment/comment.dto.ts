import { UserRO } from '../user/user.dto'
import { IdeaRO } from '../idea/idea.dto'
import { IsString } from 'class-validator'
export class CommentDTO {
  @IsString()
  readonly comment: string
}

export class CommentRO {
  readonly id: string
  readonly comment: string
  author?: UserRO
  idea?: IdeaRO
}
