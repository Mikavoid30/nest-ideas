import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  Column,
  ManyToOne,
  JoinTable
} from 'typeorm'
import { UserEntity } from '../user/user.entity'
import { IdeaEntity } from '../idea/idea.entity'

@Entity('comment')
export class CommentEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date

  @Column('text') comment: string

  @ManyToOne((user) => UserEntity)
  @JoinTable()
  author: UserEntity

  @ManyToOne((type) => IdeaEntity, (idea) => idea.comments)
  idea: IdeaEntity

  toResponseObject() {
    return {
      id: this.id,
      created: this.created,
      comment: this.comment
    }
  }
}
