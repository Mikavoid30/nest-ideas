import {
  Injectable,
  NotFoundException,
  ForbiddenException
} from '@nestjs/common'
import { IdeaEntity } from '../idea/idea.entity'
import { CommentDTO, CommentRO } from './comment.dto'
import { InjectRepository } from '@nestjs/typeorm'
import { CommentEntity } from './comment.entity'
import { Repository } from 'typeorm'
import { UserEntity } from '../user/user.entity'

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentEntity)
    private commentRepository: Repository<CommentEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    @InjectRepository(IdeaEntity)
    private ideaRepository: Repository<IdeaEntity>
  ) {}

  async showAllFromIdea(ideaId: string): Promise<CommentRO[]> {
    const idea = await this.getIdea(ideaId, [
      'comments',
      'comments.author',
      'comments.idea'
    ])
    return idea.comments.map((comment) => this.toResponseObject(comment))
  }

  async showAllFromUser(userId: string): Promise<CommentRO[]> {
    const user = await this.getUser(userId)
    const comments = await this.commentRepository.find({
      where: { author: user },
      relations: ['idea']
    })
    return comments.map((comment) => this.toResponseObject(comment))
  }

  async postToIdea(
    ideaId: string,
    authorId: string,
    data: CommentDTO
  ): Promise<CommentRO> {
    const user = await this.getUser(authorId)
    const idea = await this.getIdea(ideaId)

    const newComment = this.commentRepository.create({
      ...data,
      idea,
      author: user
    })

    await this.commentRepository.save(newComment)
    return this.toResponseObject(newComment)
  }

  async destroy(commentId: string, userId: string): Promise<{ deleted: any }> {
    const comment = await this.getComment(commentId, ['author'])
    if (comment.author.id !== userId) {
      throw new ForbiddenException("Cannot delete a comment you don't own")
    }
    await this.commentRepository.delete(comment.id)

    return { deleted: comment.id }
  }

  async read(commentId: string): Promise<CommentRO> {
    const comment = await this.getComment(commentId, ['author', 'idea'])
    return this.toResponseObject(comment)
  }

  private async getIdea(id: string, relations?: string[]): Promise<IdeaEntity> {
    try {
      return await this.ideaRepository.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException('Idea not found')
    }
  }

  private async getComment(
    id: string,
    relations?: string[]
  ): Promise<CommentEntity> {
    try {
      return await this.commentRepository.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException('Comment not found')
    }
  }

  private async getUser(id: string, relations?: string[]): Promise<UserEntity> {
    try {
      return await this.userRepository.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException('User not found')
    }
  }

  private toResponseObject({
    author,
    idea,
    ...rest
  }: CommentEntity): CommentRO {
    const res: any = { ...rest }
    if (author) res.author = author.toResponseObject(false)
    if (idea) res.idea = idea.toResponseObject()
    return res
  }
}
