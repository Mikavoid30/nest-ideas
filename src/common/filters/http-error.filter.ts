import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Logger,
  HttpException
} from '@nestjs/common'
import { Response, Request } from 'express'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { QueryFailedError } from 'typeorm'
import { HttpErrorObject } from '../interfaces/HttpErrorMessage'

@Catch(EntityNotFoundError, QueryFailedError, Error)
export class HttpErrorFilter implements ExceptionFilter {
  catch(
    exception: EntityNotFoundError | QueryFailedError | Error,
    host: ArgumentsHost
  ) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse<Response>()
    const { url: path, method } = ctx.getRequest<Request>()
    const statusCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpErrorFilter.getCustomStatus(exception)

    let { message }: { message: string | any } = exception || { message: null }
    let errorMessage =
      message instanceof Object ? message.message : exception.message

    const errorObject: HttpErrorObject = {
      statusCode,
      timestamp: new Date().toISOString(),
      path,
      method,
      errors: errorMessage
    }

    // Log the error in console
    Logger.error(
      `[${method}][${path}][${statusCode}]`,
      exception.message,
      'HttpErrorFilter'
    )

    response.status(statusCode).json(errorObject)
  }

  private static getCustomStatus(exception: Error) {
    let status = HttpStatus.INTERNAL_SERVER_ERROR
    if (exception instanceof EntityNotFoundError)
      status = HttpStatus.UNPROCESSABLE_ENTITY
    if (exception instanceof QueryFailedError) status = HttpStatus.NOT_FOUND
    return status
  }
}
