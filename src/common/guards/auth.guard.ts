import {
  Injectable,
  CanActivate,
  ExecutionContext,
  ForbiddenException
} from '@nestjs/common'
import * as jwt from 'jsonwebtoken'

@Injectable()
export class AuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest()
    try {
      const decoded = await this.validateRequest(request)
      console.log('decoded', decoded)
      return !!decoded
    } catch (err) {
      throw new ForbiddenException(err.message)
    }
  }

  async validateRequest(request: any) {
    if (!request.headers.authorization) return false
    request.user = await this.validateToken(request.headers.authorization)
    return request.user
  }

  async validateToken(auth: string) {
    const [head, token] = auth.split(' ')
    if (head !== 'Bearer' || !token) {
      throw new ForbiddenException('Invalid token')
    }
    try {
      return await jwt.verify(token, process.env.SECRET)
    } catch (err) {
      throw new ForbiddenException(err.message)
    }
  }
}
