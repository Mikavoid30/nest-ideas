import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException
} from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!value || (value instanceof Object && this.isEmpty(value))) {
      throw new BadRequestException('Validation Failed')
    }

    if (!metatype || !this.toValidate(metatype)) return value

    const instance = plainToClass(metatype, value)
    const errors = await validate(instance)
    console.log('ERRORS', errors)
    if (errors.length > 0) {
      throw new BadRequestException(
        errors.map((x) => ({ property: x.property, error: x.constraints }))
      )
    }
    return value
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array]
    return !types.includes(metatype)
  }

  private isEmpty(values: Object) {
    return Object.keys(values).length <= 0
  }
}
