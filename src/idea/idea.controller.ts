import { IdeaDTO } from './idea.dto'
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  ValidationPipe,
  UsePipes,
  UseGuards,
  Query
} from '@nestjs/common'
import { IdeaService } from './idea.service'
import { ValidationPipe as CustomValidationPipe } from '../common/pipes/validation.pipe'
import { User } from 'src/user/user.decorator'
import { AuthGuard } from '../common/guards/auth.guard'

@Controller('api/ideas')
export class IdeaController {
  constructor(private ideaService: IdeaService) {}

  @Get()
  showAllIdeas(@Query('page') page: number, @Query('take') take: number) {
    return this.ideaService.showAll(page, take)
  }

  @Get('/newest')
  showLatest(@Query('page') page: number) {
    return this.ideaService.showAll(page, 3, true)
  }

  @Post()
  @UseGuards(AuthGuard)
  @UsePipes(CustomValidationPipe)
  createIdea(@Body() data: IdeaDTO, @User('id') userId) {
    return this.ideaService.create(data, userId)
  }

  @Get(':id')
  readIdea(@Param('id') id: string) {
    return this.ideaService.read(id)
  }

  @Put(':id')
  @UseGuards(AuthGuard)
  updateIdea(
    @Param('id') id: string,
    @User('id') userId: string,
    @Body(new ValidationPipe({ skipMissingProperties: true })) data: IdeaDTO
  ) {
    return this.ideaService.update(id, data, userId)
  }

  @Delete(':id')
  @UseGuards(AuthGuard)
  destroyIdea(@Param('id') id: string, @User('id') userId: string) {
    return this.ideaService.destroy(id, userId)
  }

  @Post(':id/bookmark')
  @UseGuards(AuthGuard)
  bookmarkIdea(@Param('id') ideaId: string, @User('id') userId: string) {
    return this.ideaService.bookmark(ideaId, userId)
  }

  @Delete(':id/bookmark')
  @UseGuards(AuthGuard)
  unBookmarkIdea(@Param('id') ideaId: string, @User('id') userId: string) {
    return this.ideaService.unBookmark(ideaId, userId)
  }

  @Post(':id/upvotes')
  @UseGuards(AuthGuard)
  upvoteIdea(@Param('id') ideaId: string, @User('id') userId: string) {
    return this.ideaService.upvote(ideaId, userId)
  }

  @Post(':id/downvotes')
  @UseGuards(AuthGuard)
  downvoteIdea(@Param('id') ideaId: string, @User('id') userId: string) {
    return this.ideaService.downvote(ideaId, userId)
  }
}
