import { IsString, Length } from 'class-validator'
import { UserRO } from '../user/user.dto'

export class IdeaDTO {
  @IsString()
  @Length(5, 100)
  readonly idea!: string

  @IsString()
  @Length(5, 500)
  readonly description!: string
}

export class IdeaRO {
  id: string
  created: Date
  updated: Date
  author: UserRO
  idea: string
  description: string
  upvotes: number
  downvotes: number
}
