import { UserEntity } from '../user/user.entity'
import { CommentEntity } from '../comment/comment.entity'
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany
} from 'typeorm'

@Entity('idea')
export class IdeaEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('text') idea: string
  @Column('text') description: string

  @ManyToOne((type) => UserEntity, (author) => author.ideas)
  author: UserEntity

  @ManyToMany((type) => UserEntity, { cascade: true })
  @JoinTable()
  upvotes: UserEntity[]

  @ManyToMany((type) => UserEntity, { cascade: true })
  @JoinTable()
  downvotes: UserEntity[]

  @OneToMany((type) => CommentEntity, (comment) => comment.idea, {
    cascade: true
  })
  comments: CommentEntity[]

  toResponseObject(): Partial<IdeaEntity> {
    return {
      id: this.id,
      created: this.created,
      idea: this.idea,
      description: this.description
    }
  }
}
