import { Module } from '@nestjs/common'
import { IdeaController } from './idea.controller'
import { IdeaService } from './idea.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { IdeaEntity } from './idea.entity'
import { UserEntity } from '../user/user.entity'
import { CommentEntity } from '../comment/comment.entity'

@Module({
  imports: [TypeOrmModule.forFeature([IdeaEntity, UserEntity, CommentEntity])],
  controllers: [IdeaController],
  providers: [IdeaService]
})
export class IdeaModule {}
