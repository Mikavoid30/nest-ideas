import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
  BadRequestException
} from '@nestjs/common'
import { Repository } from 'typeorm'
import { IdeaEntity } from './idea.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { IdeaDTO, IdeaRO } from './idea.dto'
import { UserEntity } from '../user/user.entity'
import { Votes } from '../common/enums/votes.enum'

@Injectable()
export class IdeaService {
  constructor(
    @InjectRepository(IdeaEntity)
    private ideaRepository: Repository<IdeaEntity>,
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>
  ) {}

  private async vote(
    idea: IdeaEntity,
    user: UserEntity,
    vote: Votes
  ): Promise<IdeaEntity> {
    const oppositeVote: Votes = vote === Votes.UP ? Votes.DOWN : Votes.UP
    if (idea[vote].some((vote) => vote.id === user.id)) {
      throw new BadRequestException(`Already voted: ${vote}`)
    }
    idea[vote].push(user)

    // If the user has voted the opposite, remove it
    if (idea[oppositeVote].some((vote) => vote.id === user.id)) {
      idea[oppositeVote] = idea[oppositeVote].filter(
        (vote) => vote.id !== user.id
      )
    }

    return idea
  }

  private toResponseObject(idea: Partial<IdeaEntity>): IdeaRO {
    const res: any = {
      ...idea,
      author: idea.author && idea.author.toResponseObject(false)
    }
    if (res.comments)
      res.comments = idea.comments.map((comment) => comment.toResponseObject())
    if (res.upvotes) res.upvotes = idea.upvotes.length
    if (res.downvotes) res.downvotes = idea.downvotes.length
    return res
  }

  private ensureOwnership(idea: IdeaEntity, userId: string) {
    if (idea.author.id !== userId) {
      throw new UnauthorizedException('Incorrect user')
    }
    return true
  }

  async create(data: IdeaDTO, userId: string): Promise<IdeaRO> {
    const user = await this.userRepository.findOne({ id: userId })
    const idea = await this.ideaRepository.create(data)
    idea.author = user
    this.ideaRepository.save(idea)
    return this.toResponseObject(idea)
  }

  async showAll(
    page: number = 1,
    take: number = 25,
    newest: boolean = false
  ): Promise<IdeaRO[]> {
    const ideas: IdeaEntity[] = await this.ideaRepository.find({
      relations: ['author', 'upvotes', 'downvotes', 'comments'],
      take,
      skip: take * (page - 1),
      order: newest && { created: 'DESC' }
    })
    return ideas.map((idea) => this.toResponseObject(idea))
  }

  async read(id: string): Promise<IdeaRO> {
    const idea = await this.ideaRepository.findOne({
      where: { id },
      relations: ['author', 'upvotes', 'downvotes']
    })
    if (!idea) {
      throw new NotFoundException(`cannot found idea n°${id}`)
    }
    return this.toResponseObject(idea)
  }

  async update(
    id: string,
    data: Partial<IdeaDTO>,
    userId: string
  ): Promise<IdeaRO> {
    const user = await this.userRepository.findOne({ id: userId })
    const idea = await this.ideaRepository.findOne({
      where: { id },
      relations: ['author']
    })
    this.ensureOwnership(idea, user.id)
    if (!user || !idea) {
      throw new NotFoundException(`cannot found idea n°${id}`)
    }
    await this.ideaRepository.save({ ...idea, ...data })
    return this.toResponseObject({ ...idea, ...data })
  }

  async destroy(id: string, userId: string): Promise<{ deleted: string }> {
    const toDelete = await this.ideaRepository.findOne({
      where: { id },
      relations: ['author']
    })
    this.ensureOwnership(toDelete, userId)
    if (!toDelete) {
      throw new NotFoundException(`cannot found idea n°${id}`)
    }
    await this.ideaRepository.delete(id)
    return { deleted: toDelete.id }
  }

  async bookmark(ideaId: string, userId: string) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['bookmarks']
    })
    if (!user) {
      throw new UnauthorizedException()
    }
    const idea = await this.ideaRepository.findOne({ where: { id: ideaId } })
    if (!idea) {
      throw new NotFoundException(`cannot found idea n°${ideaId}`)
    }

    if (user.bookmarks.some((bookmark) => bookmark.id === idea.id)) {
      throw new BadRequestException('Bookmark already exists')
    }

    user.bookmarks.push(idea)
    await this.userRepository.save(user)
    return this.toResponseObject(idea)
  }

  async unBookmark(ideaId: string, userId: string) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['bookmarks']
    })

    // If user does not exist, unauthorized error
    if (!user) {
      throw new UnauthorizedException()
    }
    const idea = await this.ideaRepository.findOne({
      where: { id: ideaId }
    })

    // If no idea, no bookmark
    if (!idea) {
      throw new NotFoundException(`cannot found idea n°${ideaId}`)
    }

    // If this idea is not on our user bookmark list - bad request
    if (!user.bookmarks.some((bookmark) => bookmark.id === idea.id))
      throw new BadRequestException('Bookmark does not exist exists')

    // Remove the idea from the bookmark list
    user.bookmarks = user.bookmarks.filter(
      (bookmark) => bookmark.id !== idea.id
    )

    // Save the user
    await this.userRepository.save(user)

    // Return the idea
    return this.toResponseObject(idea)
  }

  async upvote(ideaId: string, userId: string): Promise<IdeaRO> {
    const user = await this.userRepository.findOne({
      where: { id: userId }
    })
    if (!user) {
      throw new UnauthorizedException()
    }
    const idea = await this.ideaRepository.findOne({
      where: { id: ideaId },
      relations: ['upvotes', 'downvotes']
    })

    if (!idea) {
      throw new NotFoundException(`cannot found idea n°${ideaId}`)
    }
    const newIdea = await this.vote(idea, user, Votes.UP)
    await this.ideaRepository.save(newIdea)
    return this.toResponseObject(newIdea)
  }

  async downvote(ideaId: string, userId: string): Promise<IdeaRO> {
    const user = await this.userRepository.findOne({
      where: { id: userId }
    })
    if (!user) {
      throw new UnauthorizedException()
    }
    const idea = await this.ideaRepository.findOne({
      where: { id: ideaId },
      relations: ['downvotes', 'upvotes']
    })
    if (!idea) {
      throw new NotFoundException(`cannot found idea n°${ideaId}`)
    }

    const newIdea = await this.vote(idea, user, Votes.DOWN)
    await this.ideaRepository.save(idea)
    return this.toResponseObject(newIdea)
  }
}
