import { config as dotenvConfig } from 'dotenv'

import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { Logger } from '@nestjs/common'

const isDev = process.env.NODE_ENV !== 'production'
if (isDev) dotenvConfig()
const port = process.env.PORT || 3000

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  // here goes the global middlewares
  await app.listen(port, () => {
    Logger.log(`Listening on port ${port}`, 'Bootstrap')
  })
}
bootstrap()
