import { Controller, Get, Post, Body, UseGuards, Query } from '@nestjs/common'
import { UserService } from './user.service'
import UserDTO from './user.dto'
import { AuthGuard } from '../common/guards/auth.guard'
import { User } from './user.decorator'

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('api/users')
  @UseGuards(AuthGuard)
  showAllUsers(@Query('page') page: number, @Query('take') take: number) {
    return this.userService.showAll(page, take)
  }

  @Post('login')
  login(@Body() data: UserDTO) {
    return this.userService.login(data)
  }

  @Post('register')
  register(@Body() data: UserDTO) {
    return this.userService.register(data)
  }
}
