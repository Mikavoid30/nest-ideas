import { IsNotEmpty } from 'class-validator'
import { IdeaEntity } from '../idea/idea.entity'

export default class UserDTO {
  @IsNotEmpty()
  readonly username: string

  @IsNotEmpty()
  readonly password: string
}

export class UserRO {
  id: string
  created: Date
  username: string
  token?: string
  ideas?: IdeaEntity[]
  bookmarks?: IdeaEntity[]
}
