import {
  Injectable,
  UsePipes,
  BadRequestException,
  ForbiddenException
} from '@nestjs/common'
import { UserEntity } from './user.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import UserDTO from './user.dto'
import { ValidationPipe as CustomValidationPipe } from '../common/pipes/validation.pipe'
import { UserRO } from './user.dto'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>
  ) {}

  @UsePipes(new CustomValidationPipe())
  async login(data: UserDTO): Promise<UserRO> {
    const { username, password } = data
    const user = await this.userRepository.findOne({ where: { username } })
    if (!user || !(await user.comparePassword(password))) {
      throw new ForbiddenException()
    }
    return user.toResponseObject()
  }

  @UsePipes(new CustomValidationPipe())
  async register(data: UserDTO): Promise<UserRO> {
    const { username } = data
    let user = await this.userRepository.findOne({
      where: { username }
    })
    if (user) {
      throw new BadRequestException('User already exists')
    }
    user = await this.userRepository.create(data)
    await this.userRepository.save(user)
    return user.toResponseObject()
  }

  async showAll(page: number = 1, take: number = 25): Promise<UserRO[]> {
    const users = await this.userRepository.find({
      relations: ['ideas', 'bookmarks'],
      take,
      skip: (page - 1) * take
    })
    return users.map((user) => user.toResponseObject(false))
  }
}
